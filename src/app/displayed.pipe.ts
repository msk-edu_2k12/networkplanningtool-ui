import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'displayed'
})
export class DisplayedPipe implements PipeTransform {


  public notAllowedParams = [
    'id',
    'name',
    'description',
    'owner',
    'sheets',
    'members'
  ]

  transform(value: any[], ...args: any[]): any {
    return value
        .filter(item => !this.notAllowedParams.includes(item.key))
        .sort((a,b) => {
          return a.key === 'createdAt' || a.key === 'updatedAt' ? -1 : 1;
        })
        .map(item => ({
          value: item.key === 'createdAt' || item.key === 'updatedAt' 
              ? new Date(item.value).toLocaleString().split(',')[0] 
              : item.value,
          displayedName: item.key.split(/(?=[A-Z])/).join(" ").capitalize()
        }));
  }

}
