import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProjectsComponent} from './projects/projects.component';
import {LoginComponent} from './login/login.component';
import {AuthGuardModule} from './auth-guard/auth-guard.module';
import { SingleProjectPageComponent } from '../app/single-project-page/single-project-page.component';
import {LogsHistoryComponent} from './logs-history/logs-history.component';


const routes: Routes = [
  {path: 'projects', component: ProjectsComponent, canActivate: [AuthGuardModule]},
  {path: 'logs', component: LogsHistoryComponent, canActivate: [AuthGuardModule]},
  {path: 'project/:id', component: SingleProjectPageComponent, canActivate: [AuthGuardModule]},
  {path: '', redirectTo: '/projects', pathMatch: 'full'},
  {path: 'login', component: LoginComponent}
];

@NgModule({ 
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
