import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {ProjectsComponent} from './projects/projects.component';
import {ClarityModule} from '@clr/angular';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './login/login.component';
import {JwtInterceptor} from './_helpers/jwt.interceptor';
import { SingleProjectPageComponent } from './single-project-page/single-project-page.component';
import { ParameterComponent } from './single-project-page/project-header/parameter/parameter.component';
import { ParametersComponent } from './single-project-page/project-header/parameters/parameters.component';
import { ProjectHeaderParametersComponent } from './single-project-page/project-header/project-header-parameters/project-header-parameters.component';
import { DisplayedPipe } from './displayed.pipe';
import { LibrarySidebarComponent } from './single-project-page/sidebar//library-sidebar/library-sidebar.component';
import { IconComponent } from './single-project-page/sidebar/icon/icon.component';
import { IconsComponent } from './single-project-page/sidebar/icons/icons.component';
import { ViewComponent } from './single-project-page/work-area/view/view.component';
import { LogsHistoryComponent } from './logs-history/logs-history.component';
import { MembersComponent } from './single-project-page/sidebar/members/members.component';
import { ModelInfoComponent } from './single-project-page/work-area/view/model-info/model-info.component';

@NgModule({
  declarations: [
    AppComponent,
    ProjectsComponent,
    LoginComponent,
    SingleProjectPageComponent,
    ParameterComponent,
    ParametersComponent,
    ProjectHeaderParametersComponent,
    DisplayedPipe,
    LibrarySidebarComponent,
    IconComponent,
    IconsComponent,
    ViewComponent,
    LogsHistoryComponent,
    MembersComponent,
    ModelInfoComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ClarityModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
