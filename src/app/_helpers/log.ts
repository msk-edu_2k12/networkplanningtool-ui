
export class Log {
  fileName: string;
  type: string;
  updatedAt: Date;
  createdAt: Date;
  username: string;
  jsonRepresentation: string;
  id: number;
}
