import {User} from './user';

export class Project {
  id: string;
  name: string;
  description: string;
  type: string;
  status: string;
  createdAt: Date;
  updatedAt: Date;
  owner: any;
  sheets: Sheet[];
  members: User[];
}

export class Sheet {
  id: string;
  name: string;
  viewId: string;
}
