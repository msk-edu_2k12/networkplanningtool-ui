import {Injectable} from '@angular/core';
import {Log} from '../_helpers/log';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class LogService {

  private logsUrl = `${environment.apiUrl}/models`;

  constructor(
    private http: HttpClient
  ) {
  }

  uploadModel(
    file: FormData,
    type: string
  ): Observable<string> {
    return this.http.post(
      `${this.logsUrl}?type=${type}`, file,
      {responseType: 'text'}
    );
  }

  getLogs(): Observable<Log[]> {
    return this.http.get<Log[]>(this.logsUrl);
  }

  getYangById(id: number): Observable<string> {
    const url = `${this.logsUrl}/${id}/content`;
    return this.http.get(url, {responseType: 'text'});
  }
}
