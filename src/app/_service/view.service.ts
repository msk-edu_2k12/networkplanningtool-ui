import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ViewService {
  private viewsUrl = `${environment.apiUrl}/storage/views`;

  constructor(
    private http: HttpClient
  ) {}

  getView(viewId: Number): Observable<any> {
    const url = `${this.viewsUrl}/${viewId}`;
    return this.http.get<any>(url);
  }

  setView(viewId: Number, json: any): Observable<any> {    
    const url = `${this.viewsUrl}/${viewId}`;
    return this.http.put<any>(url, json);
  }

  deleteProject(viewId: Number): Observable<any> {
    const url = `${this.viewsUrl}/${viewId}`;
    return this.http.delete<any>(url);
  }
}
