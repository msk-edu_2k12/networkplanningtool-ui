import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Project} from '../_helpers/project';
import {environment} from '../../environments/environment';
import {User} from '../_helpers/user';

@Injectable({providedIn: 'root'})
export class ProjectService {

  private projectsUrl = `${environment.apiUrl}/projects`;
  private usersUrl = `${environment.apiUrl}/users`

  constructor(
    private http: HttpClient
  ) {
  }

  getProjects(): Observable<Project[]> {
    const url = `${this.usersUrl}/projects`
    return this.http.get<Project[]>(url);
  }

  getProject(id: string): Observable<Project> {
    const url = `${this.projectsUrl}/${id}`;
    return this.http.get<Project>(url);
  }

  addProject(project: Project, user: User): Observable<Project> {
    const url = this.projectsUrl + '/create?ownerId=' + user.id;
    return this.http.post<Project>(url, project);
  }

  deleteProject(id: string): Observable<Project> {
    const url = `${this.projectsUrl}/${id}`;
    return this.http.delete<Project>(url);
  }

  deleteMember(projectId: string, member: User) {
    const url = `${this.projectsUrl}/${projectId}/members/${member.id}`;
    return this.http.delete(url);
  }
}
