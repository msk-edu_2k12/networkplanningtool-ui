import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IconsService {

  constructor() { }

  getIconSvg(icon: any) : string {
    let template;
    switch (`${icon.type}`) {
          case 'pc':{
            template = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n' +
              '\t viewBox="0 0 508 508" style="enable-background:new 0 0 508 508;" xml:space="preserve" width="50px" height="50px">\n' +
              '<circle style="fill:#90DFAA;" cx="254" cy="254" r="254"/>\n' +
              '<path style="fill:#2C9984;" d="M37.6,386.8C82,459.6,162.4,508,254,508s172-48.4,216.4-121.2H37.6z"/>\n' +
              '<path style="fill:#324A5E;" d="M90.8,128.8v212.8c0,4.8,4,8.8,8.8,8.8h309.2c4.8,0,8.8-4,8.8-8.8V128.8c0-4.8-4-8.8-8.8-8.8H99.2\n' +
              '\tC94.4,120.4,90.8,124,90.8,128.8z"/>\n' +
              '<rect x="109.6" y="139.2" style="fill:#54C0EB;" width="288.8" height="192.4"/>\n' +
              '<path style="fill:#E6E9EE;" d="M408.8,387.6H99.2c-12.8,0-23.2-10.4-23.2-23.2V350h356v14.4C432,377.2,421.6,387.6,408.8,387.6z"/>\n' +
              '<polygon style="fill:#ACB3BA;" points="308,350.4 204.4,350.4 216.8,366.8 295.6,366.8 "/>\n' +
              '</svg>\n'
            break;
          }
          case `firewall`: {
            template =  '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n' +
              '\t viewBox="0 0 508 508" style="enable-background:new 0 0 508 508;" xml:space="preserve" width="50px" height="50px">\n' +
              '<circle style="fill:#324A5E;" cx="254" cy="254" r="254"/>\n' +
              '<path style="fill:#E6E9EE;" d="M254,432C72.4,344,85.6,140.8,85.6,140.8C165.6,151.2,254,76,254,76s88.4,75.6,168.4,64.8\n' +
              '\tC422.4,140.8,435.6,344,254,432z"/>\n' +
              '<path style="fill:#FF7058;" d="M387.6,174.4c-1.6,18.4-5.2,43.6-14,71.2c-1.2,4-2.8,8-4,12H254V118\n' +
              '\tC282,137.6,333.2,168.4,387.6,174.4z"/>\n' +
              '<g>\n' +
              '\t<path style="fill:#FFFFFF;" d="M369.2,257.2c-22,60.4-60.8,106.4-115.2,137.2V257.2H369.2z"/>\n' +
              '\t<path style="fill:#FFFFFF;" d="M254,257.2H138.8c-12-32-16.8-61.6-18.4-82.8c54.4-6,105.6-36.8,133.6-56.4V257.2z"/>\n' +
              '</g>\n' +
              '<path style="fill:#FF7058;" d="M254,257.2V394c-63.6-36-97.6-88.8-115.2-136.8H254z"/>\n' +
              '</svg>\n';
            break;
          }
          case `location`: {
            template =  '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n' +
              '\t viewBox="0 0 508 508" style="enable-background:new 0 0 508 508;" xml:space="preserve" width="50px" height="50px">\n' +
              '<circle style="fill:#324A5E;" cx="254" cy="254" r="254"/>\n' +
              '<g>\n' +
              '\t<path style="fill:#FFFFFF;" d="M457.2,406c5.2-7.2,10-14.4,14.8-22l-58.4-110.8L314.4,290l2,11.2l9.6,53.6l3.2,17.6l4,22.8\n' +
              '\t\tl3.2,17.6l13.6,76c31.6-12.8,59.6-31.6,83.2-55.2c4.8-4.4,8.8-9.2,13.2-14.4L457.2,406z"/>\n' +
              '\t<path style="fill:#FFFFFF;" d="M208.8,273.2l-1.6,10l-2,10.8l-4.8,29.6l0,0l-2,11.2l-15.6,94l-2.4,14.8L176,470l-4,24\n' +
              '\t\tc-11.6-4-23.2-8.8-34-14.4l0,0c-8-4-15.2-8.4-22.8-13.2c-6-4-12-8-17.6-12.8c-14-10.8-26.8-23.2-38-36.8c-5.6-6.8-11.2-14-16-21.2\n' +
              '\t\tl54.4-106L208.8,273.2z"/>\n' +
              '</g>\n' +
              '<path style="fill:#E6E9EE;" d="M350,489.2L350,489.2c-16.4,6.8-33.6,11.6-51.2,14.8l0,0c-14.8,2.8-29.6,4-44.8,4\n' +
              '\tc-28.8,0-56-4.8-82-13.6l4-24l4.4-26.4l2.4-14.8l15.6-94l2-11.2l0,0l4.8-29.6l2-10.8l1.6-10l105.6,16.8l2,11.2l9.6,53.2l3.2,17.6\n' +
              '\tl4,22.8l3.2,17.6L350,489.2z"/>\n' +
              '<polygon style="fill:#4CDBC4;" points="401.6,286.4 316.4,301.6 326,355.2 410.4,305.2 "/>\n' +
              '<g>\n' +
              '\t<polygon style="fill:#84DBFF;" points="457.2,406 416,317.2 329.2,372.8 333.2,395.6 446.8,419.6 \t"/>\n' +
              '\t<path style="fill:#84DBFF;" d="M433.2,434c-23.6,23.6-52,42.4-83.2,55.2c0,0,0,0-0.4,0l-14.4-76.4h0.8L433.2,434z"/>\n' +
              '</g>\n' +
              '<polygon style="fill:#2C9984;" points="316.4,301.6 207.2,283.2 200.4,324 326,355.2 "/>\n' +
              '<g>\n' +
              '\t<polygon style="fill:#54C0EB;" points="329.2,372.8 198.8,335.2 182.8,429.2 333.2,395.6 \t"/>\n' +
              '\t<path style="fill:#54C0EB;" d="M350,489.2c-16,6.8-33.2,11.6-50.8,14.8l0,0l-122.8-33.6l4.4-26.4L336,412.8L350,489.2z"/>\n' +
              '</g>\n' +
              '<polygon style="fill:#4CDBC4;" points="200.4,324 72.4,389.2 113.2,298 207.2,283.2 "/>\n' +
              '<g>\n' +
              '\t<path style="fill:#84DBFF;" d="M198.8,335.2l-15.6,94L97.6,454c-14-10.8-26.8-23.2-38-36.8l2-4.8L198.8,335.2z"/>\n' +
              '\t<path style="fill:#84DBFF;" d="M180.4,444l-4.4,26.4l-38,9.6l0,0c-8-4-15.2-8.4-22.8-13.2L180.4,444z"/>\n' +
              '</g>\n' +
              '<path style="fill:#FF7058;" d="M329.6,158.8c0,42-75.6,154.8-75.6,154.8s-75.6-112.8-75.6-154.8s34-75.6,75.6-75.6\n' +
              '\tS329.6,117.2,329.6,158.8z"/>\n' +
              '<circle style="fill:#FFFFFF;" cx="254" cy="156.8" r="56"/>\n' +
              '<circle style="fill:#FF7058;" cx="254" cy="156.8" r="23.6"/>\n' +
              '</svg>\n';
            break;
          }
          case `router`: {
            template =  '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n' +
              '\t viewBox="0 0 508 508" style="enable-background:new 0 0 508 508;" xml:space="preserve" width="50px" height="50px">\n' +
              '<circle style="fill:#84DBFF;" cx="254" cy="254" r="254"/>\n' +
              '<path style="fill:#54C0EB;" d="M33.2,380c43.6,76.4,126,128,220.8,128c94.4,0,176.8-51.6,220.8-128H33.2z"/>\n' +
              '<g>\n' +
              '\t<path style="fill:#324A5E;" d="M106,145.2c-1.2-3.2-4.4-4.8-7.6-3.6s-4.8,4.4-3.6,7.6l48.4,137.6c0.8,2.4,3.2,4,5.6,4\n' +
              '\t\tc0.8,0,1.2,0,2-0.4c3.2-1.2,4.8-4.4,3.6-7.6L106,145.2z"/>\n' +
              '\t<path style="fill:#324A5E;" d="M409.2,141.6c-3.2-1.2-6.4,0.4-7.6,3.6l-48.4,137.2c-1.2,3.2,0.4,6.4,3.6,7.6c0.8,0.4,1.2,0.4,2,0.4\n' +
              '\t\tc2.4,0,4.8-1.6,5.6-4l48.4-137.6C414,146,412.4,142.4,409.2,141.6z"/>\n' +
              '</g>\n' +
              '<g>\n' +
              '\t<path style="fill:#2B3B4E;" d="M411.6,360.8h-46.8v13.6c0,2.8,2.4,5.2,5.2,5.2h36c2.8,0,5.2-2.4,5.2-5.2v-13.6H411.6z"/>\n' +
              '\t<path style="fill:#2B3B4E;" d="M143.2,360.8H96.4v13.6c0,2.8,2.4,5.2,5.2,5.2h36c2.8,0,5.2-2.4,5.2-5.2v-13.6H143.2z"/>\n' +
              '</g>\n' +
              '<path style="fill:#FFFFFF;" d="M421.2,370H86.8c-6,0-11.2-5.2-11.2-11.2v-70c0-6,5.2-11.2,11.2-11.2h334.8c6,0,11.2,5.2,11.2,11.2\n' +
              '\tv70C432.4,365.2,427.6,370,421.2,370z"/>\n' +
              '<rect x="75.6" y="301.2" style="fill:#324A5E;" width="357.2" height="37.6"/>\n' +
              '<circle style="fill:#FFD05B;" cx="131.6" cy="324" r="12.8"/>\n' +
              '<path style="fill:#F9B54C;" d="M122.8,314.8c-4.8,4.8-4.8,12.8,0,18l18-18C135.6,310,127.6,310,122.8,314.8z"/>\n' +
              '<circle style="fill:#FFFFFF;" cx="266.8" cy="324" r="10"/>\n' +
              '<circle style="fill:#4CDBC4;" cx="308.8" cy="324" r="10"/>\n' +
              '<circle style="fill:#FFD05B;" cx="351.2" cy="324" r="10"/>\n' +
              '<circle style="fill:#FF7058;" cx="393.2" cy="324" r="10"/>\n' +
              '</svg>\n';
            break;
          }
          case `server`: {
            template =  '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n' +
              '\t viewBox="0 0 508 508" style="enable-background:new 0 0 508 508;" xml:space="preserve" width="50px" height="50px">\n' +
              '<circle style="fill:#84DBFF;" cx="254" cy="254" r="254"/>\n' +
              '<g>\n' +
              '\t<rect x="246.8" y="364.8" style="fill:#2B3B4E;" width="14.4" height="57.6"/>\n' +
              '\t<rect x="164.4" y="417.6" style="fill:#2B3B4E;" width="179.2" height="14.4"/>\n' +
              '</g>\n' +
              '<rect x="138" y="131.6" style="fill:#324A5E;" width="232" height="201.6"/>\n' +
              '<g>\n' +
              '\t<path style="fill:#E6E9EE;" d="M393.2,172.4H114.8c-6.4,0-12-5.2-12-12v-56c0-6.4,5.2-12,12-12h278.4c6.4,0,12,5.2,12,12v56\n' +
              '\t\tC404.8,166.8,399.6,172.4,393.2,172.4z"/>\n' +
              '\t<path style="fill:#E6E9EE;" d="M393.2,272.4H114.8c-6.4,0-12-5.2-12-12v-56c0-6.4,5.2-12,12-12h278.4c6.4,0,12,5.2,12,12v56\n' +
              '\t\tC404.8,267.2,399.6,272.4,393.2,272.4z"/>\n' +
              '\t<path style="fill:#E6E9EE;" d="M393.2,372.4H114.8c-6.4,0-12-5.2-12-12v-56c0-6.4,5.2-12,12-12h278.4c6.4,0,12,5.2,12,12v56\n' +
              '\t\tC404.8,367.2,399.6,372.4,393.2,372.4z"/>\n' +
              '</g>\n' +
              '<g>\n' +
              '\t<path style="fill:#ACB3BA;" d="M133.2,125.6L133.2,125.6c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4\n' +
              '\t\tl0,0C141.6,122,138,125.6,133.2,125.6z"/>\n' +
              '\t<path style="fill:#ACB3BA;" d="M133.2,156L133.2,156c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4l0,0\n' +
              '\t\tC141.6,152.4,138,156,133.2,156z"/>\n' +
              '\t<path style="fill:#ACB3BA;" d="M161.6,125.6L161.6,125.6c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4\n' +
              '\t\tl0,0C170,122,166,125.6,161.6,125.6z"/>\n' +
              '\t<path style="fill:#ACB3BA;" d="M161.6,156L161.6,156c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4l0,0\n' +
              '\t\tC170,152.4,166,156,161.6,156z"/>\n' +
              '\t<path style="fill:#ACB3BA;" d="M189.6,125.6L189.6,125.6c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4\n' +
              '\t\tl0,0C198.4,122,194.4,125.6,189.6,125.6z"/>\n' +
              '\t<path style="fill:#ACB3BA;" d="M189.6,156L189.6,156c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4l0,0\n' +
              '\t\tC198.4,152.4,194.4,156,189.6,156z"/>\n' +
              '\t<rect x="287.2" y="112" style="fill:#ACB3BA;" width="97.6" height="5.2"/>\n' +
              '\t<rect x="287.2" y="124" style="fill:#ACB3BA;" width="97.6" height="5.2"/>\n' +
              '\t<rect x="287.2" y="135.6" style="fill:#ACB3BA;" width="97.6" height="5.2"/>\n' +
              '\t<rect x="287.2" y="147.6" style="fill:#ACB3BA;" width="97.6" height="5.2"/>\n' +
              '</g>\n' +
              '<circle style="fill:#FF7058;" cx="237.2" cy="132.4" r="12"/>\n' +
              '<g>\n' +
              '\t<path style="fill:#ACB3BA;" d="M133.2,226L133.2,226c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4l0,0\n' +
              '\t\tC141.6,222,138,226,133.2,226z"/>\n' +
              '\t<path style="fill:#ACB3BA;" d="M133.2,256L133.2,256c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4l0,0\n' +
              '\t\tC141.6,252.4,138,256,133.2,256z"/>\n' +
              '\t<path style="fill:#ACB3BA;" d="M161.6,226L161.6,226c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4l0,0\n' +
              '\t\tC170,222,166,226,161.6,226z"/>\n' +
              '\t<path style="fill:#ACB3BA;" d="M161.6,256L161.6,256c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4l0,0\n' +
              '\t\tC170,252.4,166,256,161.6,256z"/>\n' +
              '\t<path style="fill:#ACB3BA;" d="M189.6,226L189.6,226c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4l0,0\n' +
              '\t\tC198.4,222,194.4,226,189.6,226z"/>\n' +
              '\t<path style="fill:#ACB3BA;" d="M189.6,256L189.6,256c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4l0,0\n' +
              '\t\tC198.4,252.4,194.4,256,189.6,256z"/>\n' +
              '\t<rect x="287.2" y="212" style="fill:#ACB3BA;" width="97.6" height="5.2"/>\n' +
              '\t<rect x="287.2" y="224" style="fill:#ACB3BA;" width="97.6" height="5.2"/>\n' +
              '\t<rect x="287.2" y="236" style="fill:#ACB3BA;" width="97.6" height="5.2"/>\n' +
              '\t<rect x="287.2" y="247.6" style="fill:#ACB3BA;" width="97.6" height="5.2"/>\n' +
              '</g>\n' +
              '<circle style="fill:#FFD05B;" cx="237.2" cy="232.4" r="12"/>\n' +
              '<g>\n' +
              '\t<path style="fill:#ACB3BA;" d="M133.2,326L133.2,326c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4l0,0\n' +
              '\t\tC141.6,322,138,326,133.2,326z"/>\n' +
              '\t<path style="fill:#ACB3BA;" d="M133.2,356.4L133.2,356.4c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4\n' +
              '\t\tl0,0C141.6,352.4,138,356.4,133.2,356.4z"/>\n' +
              '\t<path style="fill:#ACB3BA;" d="M161.6,326L161.6,326c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4l0,0\n' +
              '\t\tC170,322,166,326,161.6,326z"/>\n' +
              '\t<path style="fill:#ACB3BA;" d="M161.6,356.4L161.6,356.4c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4\n' +
              '\t\tl0,0C170,352.4,166,356.4,161.6,356.4z"/>\n' +
              '\t<path style="fill:#ACB3BA;" d="M189.6,326L189.6,326c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4l0,0\n' +
              '\t\tC198.4,322,194.4,326,189.6,326z"/>\n' +
              '\t<path style="fill:#ACB3BA;" d="M189.6,356.4L189.6,356.4c-4.8,0-8.4-4-8.4-8.4l0,0c0-4.8,4-8.4,8.4-8.4l0,0c4.8,0,8.4,4,8.4,8.4\n' +
              '\t\tl0,0C198.4,352.4,194.4,356.4,189.6,356.4z"/>\n' +
              '\t<rect x="287.2" y="312.4" style="fill:#ACB3BA;" width="97.6" height="5.2"/>\n' +
              '\t<rect x="287.2" y="324" style="fill:#ACB3BA;" width="97.6" height="5.2"/>\n' +
              '\t<rect x="287.2" y="336" style="fill:#ACB3BA;" width="97.6" height="5.2"/>\n' +
              '\t<rect x="287.2" y="348" style="fill:#ACB3BA;" width="97.6" height="5.2"/>\n' +
              '</g>\n' +
              '<circle style="fill:#4CDBC4;" cx="237.2" cy="332.8" r="12"/>\n' +
              '<circle style="fill:#324A5E;" cx="254" cy="424.8" r="24.8"/>\n' +
              '</svg>\n';
            break;
          }
          case `switch`: {
            template = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n' +
              '\t viewBox="0 0 508 508" style="enable-background:new 0 0 508 508;" xml:space="preserve" width="50px" height="50px">\n' +
              '<circle style="fill:#FD8469;" cx="254" cy="254" r="254"/>\n' +
              '<g>\n' +
              '\t<path style="fill:#324A5E;" d="M334,335.6v-66h-16v74c0,4.4,3.6,8,8,8h99.6v-16H334z"/>\n' +
              '\t<path style="fill:#324A5E;" d="M304.8,269.6h-16v106c0,4.4,3.6,8,8,8h99.6v-16h-91.6V269.6z"/>\n' +
              '\t<path style="fill:#324A5E;" d="M189.6,343.6v-74h-16v66H82.4v16H182C186,351.6,189.6,348,189.6,343.6z"/>\n' +
              '\t<path style="fill:#324A5E;" d="M203.2,367.2h-91.6v16h99.6c4.4,0,8-3.6,8-8V269.6h-16V367.2z"/>\n' +
              '\t<rect x="246" y="269.6" style="fill:#324A5E;" width="16" height="149.2"/>\n' +
              '</g>\n' +
              '<path style="fill:#E6E9EE;" d="M358.4,168.8c0-1.2,0-2.4,0-3.6c0-26-21.2-47.2-47.2-47.2c-11.2,0-21.6,4-29.6,10.4\n' +
              '\tc-12-22.8-36-38.8-63.6-38.8c-39.6,0-71.2,32-71.2,71.2c0,2.8,0,5.2,0.4,8c-29.2,2.8-52.4,27.6-52.4,57.6c0,32,26,57.6,57.6,57.6\n' +
              '\th202.8c32,0,57.6-26,57.6-57.6C413.2,195.6,388.8,170.4,358.4,168.8z"/>\n' +
              '</svg>\n';
            break;
          }
          case 'cloud': {
            template = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n' +
              '\t viewBox="0 0 508 508" style="enable-background:new 0 0 508 508;" xml:space="preserve" width="50px" height="50px">\n' +
              '<circle style="fill:#FD8469;" cx="254" cy="254" r="254"/>\n' +
              '<g>\n' +
              '\t<rect x="150" y="373.6" style="fill:#324A5E;" width="208" height="15.6"/>\n' +
              '\t<rect x="246.4" y="295.2" style="fill:#324A5E;" width="15.6" height="87.2"/>\n' +
              '</g>\n' +
              '<path style="fill:#FFFFFF;" d="M372.8,185.6c0-1.6,0.4-2.8,0.4-4.4c0-30-24-54-54-54c-12.8,0-24.4,4.4-34,12c-13.6-26-40.8-44-72-44\n' +
              '\tc-44.8,0-81.2,36.4-81.2,81.2c0,3.2,0,6,0.4,9.2c-33.6,3.2-59.6,31.2-59.6,65.6c0,36.4,29.6,65.6,65.6,65.6h231.2\n' +
              '\tc36.4,0,65.6-29.6,65.6-65.6C435.2,216,407.6,187.2,372.8,185.6z"/>\n' +
              '<circle style="fill:#E6E9EE;" cx="254" cy="381.2" r="31.6"/>\n' +
              '</svg>\n';
            break;
          }
      case 'hub': {
            template = '<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 508 508" width="50px" height="50px"><defs><style>.cls-1{fill:#84dbff;}.cls-2{fill:#e6e9ee;}.cls-3{fill:#324a5e;}.cls-4{fill:#ffd05b;}</style></defs><title>connector</title><circle class="cls-1" cx="254" cy="254" r="254"/><path class="cls-2" d="M192.57,239.35H115.43a2.49,2.49,0,0,1-2.53-2.53V167.18a2.49,2.49,0,0,1,2.53-2.53h77.14a2.49,2.49,0,0,1,2.53,2.53v69.53A2.58,2.58,0,0,1,192.57,239.35Z"/><polygon class="cls-3" points="187.28 172.47 187.28 216.44 169.65 216.44 169.65 224.04 163.26 224.04 163.26 231.53 144.74 231.53 144.74 224.04 138.35 224.04 138.35 216.44 120.72 216.44 120.72 172.47 187.28 172.47"/><rect class="cls-4" x="127.67" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="134.72" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="141.77" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="148.82" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="155.87" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="162.93" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="169.98" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="177.03" y="178.75" width="3.31" height="10.58"/><path class="cls-2" d="M292.57,239.35H215.43a2.49,2.49,0,0,1-2.53-2.53V167.18a2.49,2.49,0,0,1,2.53-2.53h77.14a2.49,2.49,0,0,1,2.53,2.53v69.53A2.58,2.58,0,0,1,292.57,239.35Z"/><polygon class="cls-3" points="287.28 172.47 287.28 216.44 269.65 216.44 269.65 224.04 263.26 224.04 263.26 231.53 244.74 231.53 244.74 224.04 238.35 224.04 238.35 216.44 220.72 216.44 220.72 172.47 287.28 172.47"/><rect class="cls-4" x="227.67" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="234.72" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="241.77" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="248.82" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="255.87" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="262.93" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="269.98" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="277.03" y="178.75" width="3.31" height="10.58"/><path class="cls-2" d="M392.57,239.35H315.43a2.49,2.49,0,0,1-2.53-2.53V167.18a2.49,2.49,0,0,1,2.53-2.53h77.14a2.49,2.49,0,0,1,2.53,2.53v69.53A2.58,2.58,0,0,1,392.57,239.35Z"/><polygon class="cls-3" points="387.28 172.47 387.28 216.44 369.65 216.44 369.65 224.04 363.26 224.04 363.26 231.53 344.74 231.53 344.74 224.04 338.35 224.04 338.35 216.44 320.72 216.44 320.72 172.47 387.28 172.47"/><rect class="cls-4" x="327.67" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="334.72" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="341.77" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="348.82" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="355.87" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="362.93" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="369.98" y="178.75" width="3.31" height="10.58"/><rect class="cls-4" x="377.03" y="178.75" width="3.31" height="10.58"/><path class="cls-2" d="M192.57,345.69H115.43a2.49,2.49,0,0,1-2.53-2.54V273.51a2.49,2.49,0,0,1,2.53-2.53h77.14a2.49,2.49,0,0,1,2.53,2.53V343A2.59,2.59,0,0,1,192.57,345.69Z"/><polygon class="cls-3" points="187.28 278.8 187.28 322.77 169.65 322.77 169.65 330.37 163.26 330.37 163.26 337.86 144.74 337.86 144.74 330.37 138.35 330.37 138.35 322.77 120.72 322.77 120.72 278.8 187.28 278.8"/><rect class="cls-4" x="127.67" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="134.72" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="141.77" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="148.82" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="155.87" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="162.93" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="169.98" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="177.03" y="285.08" width="3.31" height="10.58"/><path class="cls-2" d="M292.57,345.69H215.43a2.49,2.49,0,0,1-2.53-2.54V273.51a2.49,2.49,0,0,1,2.53-2.53h77.14a2.49,2.49,0,0,1,2.53,2.53V343A2.59,2.59,0,0,1,292.57,345.69Z"/><polygon class="cls-3" points="287.28 278.8 287.28 322.77 269.65 322.77 269.65 330.37 263.26 330.37 263.26 337.86 244.74 337.86 244.74 330.37 238.35 330.37 238.35 322.77 220.72 322.77 220.72 278.8 287.28 278.8"/><rect class="cls-4" x="227.67" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="234.72" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="241.77" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="248.82" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="255.87" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="262.93" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="269.98" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="277.03" y="285.08" width="3.31" height="10.58"/><path class="cls-2" d="M392.57,345.69H315.43a2.49,2.49,0,0,1-2.53-2.54V273.51a2.49,2.49,0,0,1,2.53-2.53h77.14a2.49,2.49,0,0,1,2.53,2.53V343A2.59,2.59,0,0,1,392.57,345.69Z"/><polygon class="cls-3" points="387.28 278.8 387.28 322.77 369.65 322.77 369.65 330.37 363.26 330.37 363.26 337.86 344.74 337.86 344.74 330.37 338.35 330.37 338.35 322.77 320.72 322.77 320.72 278.8 387.28 278.8"/><rect class="cls-4" x="327.67" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="334.72" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="341.77" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="348.82" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="355.87" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="362.93" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="369.98" y="285.08" width="3.31" height="10.58"/><rect class="cls-4" x="377.03" y="285.08" width="3.31" height="10.58"/></svg>';
            break;
      }
        }
    return template;
  }

  getSingleParamTemplate(parameter: any): string {
    return '<div class="properties" xmlns="http://www.w3.org/1999/xhtml">' + parameter.name + ': ' + parameter.value + '</div>';
  }

  getParamsTemplate(model: Array<any>): string {
    let paramsTemplate = '';
    let k = 0;
    for(let parameter of model) {
      if(parameter.name != 'name' && k < 4) {
        paramsTemplate += this.getSingleParamTemplate(parameter);
        k++;
      }
    }
    return paramsTemplate;
  }

  getNodeSvg(icon: any, clusterId: string, nodeColor: string, model: Array<any>): string {
    if(icon.type === 'cloud')
      return  '<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50"><foreignObject x="0" y="0" width="100%" height="100%"><div class="content__icon" xmlns="http://www.w3.org/1999/xhtml">' + this.getIconSvg(icon) + '</div></foreignObject></svg>';
    else return `<svg xmlns="http://www.w3.org/2000/svg" width="207" height="90">` +
      `<foreignObject x="0" y="0" width="100%" height="100%">` +
      `<div class="main" xmlns="http://www.w3.org/1999/xhtml">
        <h2 class="title">${icon.name}</h2>
        <hr />
         <div class="content" xmlns="http://www.w3.org/1999/xhtml">
            <div class="content__icon" xmlns="http://www.w3.org/1999/xhtml">` + this.getIconSvg(icon) + `</div>
            <div class="content__description" xmlns="http://www.w3.org/1999/xhtml">` + this.getParamsTemplate(model) + `</div>
         </div>
        </div>
        <style>
        .main {
            width: 200px;
            font-family: sans-serif;
            border: 2px solid;
            border-radius: 15px;
            background-color: #` + nodeColor + `;
         }
         .title {
            margin: 0;
            font-size: 20px;
            line-height: 20px;
            text-align: center;
        }
        hr {
            margin: 4px 0;
            border: none;
            background-color: black;
            height: 2px;
        }
        .content {
            display: flex;
            padding: 0 5px 5px 5px;
        }
        .content__icon {
            width: 40%;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .content__description {
            width: 60%;
            margin-left: 10px
        }
        .properties {
            font-size: 10px;
        }
        .content__description>ul {
            padding-left: 10px;
        }
        .content__description>ul li {
                margin-bottom: 5px;
        }
        .image {
            width: 60px;
            height: 60px;
        }
        </style>
        </foreignObject>
        </svg>`;
  }
}
