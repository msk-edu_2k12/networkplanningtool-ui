import { Component, OnInit, Input } from '@angular/core';
import { Project } from 'src/app/_helpers/project';

@Component({
  selector: 'app-project-header-parameters',
  templateUrl: './project-header-parameters.component.html',
  styleUrls: ['./project-header-parameters.component.css']
})
export class ProjectHeaderParametersComponent implements OnInit {

  @Input() project: Project;
  
  constructor() { }

  ngOnInit() {
  }

}
