import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {DataSet, Network} from 'vis-network';
import {IconsService} from '../../../_service/icons.service';
import {FullItem} from 'vis-data/dist/types/data-interface';
import {Project} from '../../../_helpers/project';
import {ViewService} from '../../../_service/view.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit, AfterViewInit {

  @ViewChild('myNetwork', {static: false}) container;

  private _project: Project;

  @Output() onAddLink = new EventEmitter<Object>();
  selectedItem: any = {
    basic: '',
    type: ''
  };
  @Input()
  set project(value) {
    this._project = value;
    this.viewId = this._project.sheets[0].viewId;
    this.getView();
  };

  get project(): Project {
    return this._project;
  }

  public viewId;
  public availableColors = ['DAE8FC', 'D5E8D4', 'FFE6CC', 'F8CECC', 'E1D5E7', 'F4FF8F', 'CDFF9C', 'A8FFF6'];
  public reservedColors = [];
  public networkApi;

  public edges = new DataSet([]);
  public nodes = new DataSet([]);
  public data = {
    nodes: this.nodes,
    edges: this.edges
  };

  public options = {
    physics: false,
    nodes: {
      physics: false,
    },
    edges: {
      physics: false,
      font: {
        align: 'middle'
      },
      smooth: {
        enabled: false
      }
    }
  };

  constructor(
    private viewService: ViewService,
    private iconsService: IconsService
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.networkApi = new Network(this.container.nativeElement, this.data, this.options as any);
    const options = {
      edges: {
        color: {
          color: '#448eff'
        }
      },
      interaction: {
        selectConnectedEdges: false,
      }
    };
    this.networkApi.setOptions(options);

    this.networkApi.addEventListener('controlNodeDragEnd', (event) => {
      this.onAddLink.emit(event);
    });
    this.networkApi.on('doubleClick', (params) => {
      if (params.nodes.length === 1) {
        if (this.networkApi.isCluster(params.nodes[0]) === true) {
          this.networkApi.openCluster(params.nodes[0], {
            releaseFunction: (clusterPosition, containedNodesPositions) => {
              return containedNodesPositions;
            }
          });
        }
      }
    });

    this.networkApi.on('selectNode', (params) => {
      if (this.networkApi.isCluster(params.nodes[0])) {
        this.selectedItem.basic = this.findClusterById(params.nodes[0]);
        this.selectedItem.type = 'location-node';
      }
      else {
        this.selectedItem.basic = this.findNodeById(params.nodes[0]);
        this.selectedItem.type = 'simple-node';
      }
    });

    this.networkApi.on('deselectNode', () => {
      this.selectedItem = {
        basic: '',
        type: ''
      };
    });

    this.networkApi.on('selectEdge', (params) => {
      if(params.edges[0].indexOf('clusterEdge') == -1) {
        this.selectedItem.basic = this.findLinkById(params.edges[0]);
        this.selectedItem.type = 'simple-edge';
      }
    });

    this.networkApi.on('deselectEdge', () => {
      this.selectedItem = {
        basic: '',
        type: ''
      };
    });

  }

  expand() {
    const api = this.networkApi;
    const allNodes = Object.values(api.body.nodes) as any;
    allNodes.forEach(node => {
      if (api.isCluster(node.id)) {
        api.openCluster(node.id, {
          releaseFunction: (clusterPosition, containedNodesPositions) => {
            return containedNodesPositions;
          }
        });
      }
    })
  }

  collapse(): void {
    const api = this.networkApi;
    let clusterOptionsByData;
    const clusters = this.getAllLocations();
    clusters.forEach(cluster => {
      const id = Date.now();
      clusterOptionsByData = {
        joinCondition: childOptions => childOptions.cid === cluster.cid,
        clusterNodeProperties: {id: id, cid: cluster.id, label: cluster.label, shape: 'image', image: cluster.image, params: cluster.params}
      };
      api.cluster(clusterOptionsByData);
    });
  }

  createElement(icon: any, model: Array<any>, clusterId: string, controlEdge: any) {
    let id = Date.now();
    let edgesToSave = this.edges.getDataSet().map(item => item);

    if (icon.name !== 'Link') {
      const nodeColor = this.getNodeColor(clusterId, icon.type);
      const svg = this.iconsService.getNodeSvg(icon, clusterId, nodeColor, model);
      const url = 'data:image/svg+xml;charset=utf-8,' + encodeURIComponent(svg);
      this.nodes.add({
        id: id.toString(),
        label: '',
        cid: this.getCid(icon.type, id, clusterId),
        shape: 'image',
        type: `${icon.type}`,
        image: url,
        params: model,
        color: nodeColor,
      });
      if (icon.type != 'location' && icon.type != 'cloud') {
        const currentLocation = this.findNodeById(clusterId);
        currentLocation.hidden = true;
        this.nodes.update(currentLocation);
      }
    } else {
      edgesToSave.forEach( (edge) => {
        if (edge.from == controlEdge.from && edge.to == controlEdge.to)
        edge.params = model;
      });
      this.edges.update(edgesToSave);
    }
    this.networkApi.setData(this.data);
    this.networkApi.redraw();
    this.saveView(edgesToSave);
  }

  deleteSelectedItem() {
    if (this.selectedItem.type == 'simple-node') {
      this.nodes.remove(this.selectedItem.basic.id)
    } else if (this.selectedItem.type == 'simple-edge') {
      this.edges.remove(this.selectedItem.basic.id)
    }
    this.networkApi.redraw();
    this.saveView(this.edges.getDataSet().map(item => item));
    this.selectedItem = {basic: '', type: ''};
  }


  getView(): void {
    this.viewService.getView(this.viewId)
      .subscribe(data => {
        if (data.viewData) {
          if (data.viewData.nodes) {
            data.viewData.nodes.forEach(nodeFromServer => {
              this.nodes.add(nodeFromServer);
            });
          }
          if (data.viewData.edges) {
            data.viewData.edges.forEach(edgeFromServer => {
              this.edges.add(edgeFromServer);
            });
          }
        }
        this.networkApi.setData(this.data);
        this.networkApi.redraw();
      });
  }

  saveView(edgesToSave): void {
    const nodesToSave = this.data.nodes.getDataSet().map(item => item);
    this.viewService.setView(
      this.viewId,
      {
        viewData: {
          nodes: nodesToSave,
          edges: edgesToSave
        }
      }
    ).subscribe();
  }

  getCid(type: string, currentId: number, clusterId: string): string {
    switch (type) {
      case 'location':
        return currentId.toString();
      case 'cloud':
        return null;
      default:
        return clusterId;
    }
  }

  onEdgeMode(): void  {
    this.networkApi.addEdgeMode()
  }

  canCreate(icon: any): boolean {
    if(icon.type !== 'link')
      return this.getAllLocations().length !== 0 || icon.type === 'location' || icon.type === 'cloud';
    else
      return this.nodes.length - this.getAllLocations().length > 1;
  }

  getAllLocations(): FullItem<any, 'id'>[] {
    return this.nodes.get({
      filter: function(node) {
        return node.type == 'location';
      }
    });
  }

  findNodeById(id: any) {
    return this.nodes.get({
      filter: (node) => {
        return node.id == id;
      }
    })[0];
  }

  findLinkById(id: string) {
      return this.edges.get({
        filter: (edge) => {
          return edge.id == id;
        }
      })[0];
  }

  findClusterById(id: number) {
    const api = this.networkApi;
    const allNodes = Object.values(api.body.nodes) as any[];
    const selectedLocation = allNodes.filter((node) => {
        return node.options.id === id;
      }
    )[0];
    return selectedLocation.options;
  }

  getReservedColors() {
    let locations = this.getAllLocations();
    for (let loc of locations) {
      if (!this.reservedColors.includes(loc.color)) {
        this.reservedColors.push(loc.color);
      }
    }
    return this.reservedColors;
  }

  getNodeColor(clusterId: string, type: string): string {
    let color;
    switch (type) {
      case 'location': {
        const reservedColors = this.getReservedColors();
        const resLength = reservedColors.length;
        if (resLength != 0) {
          for (let i = resLength - 1; i >= 0; i--) {
            const ind = this.availableColors.indexOf(reservedColors[i]);
            if (ind != -1) {
              this.availableColors.splice(ind, 1);
            }
          }
        }
        color = this.availableColors[0];
        this.reservedColors.push(color);
        return color;
      }
      case 'cloud': {
        return null;
      }
      default: {
        const locations = this.getAllLocations();
        const selectedLocation = locations.filter((loc) => {
          return loc.id == clusterId;
        })[0];
        return selectedLocation.color;
      }
    }
  }
}
