import {Component, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Project } from '../_helpers/project';
import { ProjectService } from '../_service/project.service';
import { ModelsService } from '../_service/models.service';

@Component({
  selector: 'app-single-project-page',
  templateUrl: './single-project-page.component.html',
  styleUrls: ['./single-project-page.component.css']
})
export class SingleProjectPageComponent implements OnInit {
  @ViewChild('view', {static: false}) view;
  project: Project;
  clickedIcon: any;
  model: any[];
  controlEdge: Object;
  clusterId: string;
  cantCreate: boolean = false;
  opened: boolean;
  availableLocations: any;
  errorMsg: string = 'Default Error Message!';

  constructor(
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private modelsService: ModelsService
  ) {
  }

   ngOnInit(): void {
    this.getProject();
  }

  getProject(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.projectService.getProject(id)
      .subscribe(project => this.project = project);
  }

  getModel(): void {
    this.modelsService.getModel(this.clickedIcon.type)
      .subscribe(model => {
        this.model = model;
        this.model.forEach((attribute) => {
          if (attribute['data-type'] == 'list') {
            attribute.value = []
          } else {
            attribute.value = null
          }
        })
      });
  }

  showError(msg: string) {
    this.errorMsg = msg;
    this.cantCreate = true;
    setTimeout(() => {
      this.cantCreate = false
    }, 5000)
  }

  onClickSidebar(icon: any) {
    if (icon.type !== 'link') {
      this.availableLocations = this.view.getAllLocations();
      this.clickedIcon = icon;
      if (this.view.canCreate(icon))  {
        this.getModel();
        this.opened = true;
      } else {
        this.showError('Add at least one Location!');
      }
    }
    else {
      if (this.view.canCreate(icon)) {
        this.view.onEdgeMode();
      }
      else {
        this.showError('Add at least two simple nodes! Then use Link.');
      }
    }
  }

  addValue(attribute) {
    let index = this.model.indexOf(attribute)
    this.model[index].value.push({});
  }

  onAddLink(event) {
    if (this.canCreateLink(event.controlEdge)) {
      this.controlEdge = event.controlEdge;
      this.clickedIcon = {name: 'Link', image: 'link.png', type: 'link'};
      if (this.view.canCreate(this.clickedIcon)) {
        this.opened = true;
        this.getModel();
      }
    }
  }
  onClickOk() {
    this.view.createElement(this.clickedIcon, this.model, this.clusterId, this.controlEdge);
    this.opened = false;
    this.model = undefined;
    this.clusterId = undefined;
  }

  canCreateLink(controlEdge: any) {
    const node1 = this.findNodeById(controlEdge.from);
    const node2 = this.findNodeById(controlEdge.to);
    if (controlEdge.from == controlEdge.to) {
      this.showError('Self-looped nodes prohibited!')
      return false;
    } else if (node1.type == 'location' || node2.type == 'location') {
      this.showError('Can not connect Location node with any other node!')
      return false;
    } else return true;
  }

  onClickCancel() {
    this.clickedIcon = null;
    this.opened = false;
    this.model = undefined;
  }

  findNodeById(id: any) {
    return this.view.nodes.get({
      filter: (node) => {
        return node.id == id;
      }
    })[0];
  }
}
