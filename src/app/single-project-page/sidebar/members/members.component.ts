import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../_helpers/user';
import {Project} from '../../../_helpers/project';
import {UserService} from '../../../_service/user.service';
import {ProjectService} from '../../../_service/project.service';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {

  members: User[] = [];
  @Input() project: Project;
  opened: boolean = false;
  availableUsers: User[];

  constructor(private userService: UserService,
              private projectService: ProjectService) { }

  ngOnInit() {
    this.getMembers();
    this.getAvailableUsers();
  }

  getMembers() {
    this.userService.getProjectMembers(this.project.id)
      .subscribe( (members) => {
        this.members = members;
      });
  }

  addMember() {
    this.getAvailableUsers();
    this.opened = true;
  }

  deleteMember(member: User) {
    this.projectService.deleteMember(this.project.id, member).subscribe(() => {
        this.getMembers();
        this.getAvailableUsers();
      }
    );
  }

  chooseUser(user: User) {
    this.userService.addMember(user.id, this.project.id).subscribe(() => {
      this.getMembers();
      this.getAvailableUsers();
    });
    this.opened = false;
  }

  getAvailableUsers() {
    this.userService.getAllUsers().subscribe( (users: User[]) => {
      this.availableUsers = users.filter((user1) => {
        return !this.members.some((user2) => {
          return user1.id === user2.id;
        }) && this.project.owner.name !== user1.name;
      });
    });
  }
}
