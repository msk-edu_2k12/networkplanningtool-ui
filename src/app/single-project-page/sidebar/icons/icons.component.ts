import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-icons',
  templateUrl: './icons.component.html',
  styleUrls: ['./icons.component.css']
})
export class IconsComponent implements OnInit {

  @Input() icons: Object;

  @Output() onClickIcons = new EventEmitter<Object>();

  constructor() { }

  ngOnInit() {
  }

  onClickIcon(icon: Object){
    this.onClickIcons.emit(icon);
  }

}
