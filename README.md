
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Mock server

Run `npm run mock` for a mock server on `http://localhost:3000/`. For details see: [article](https://medium.com/letsboot/the-perfect-mock-backend-to-start-with-an-angular-application-3d751d16614f)


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
